package stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;

public class TestCaseOneDefinition {

    int age;
    boolean result;


    @Before
    public  void m1(){


        System.out.println("this is before hook m1 method");
    }
    @Before("@smoke")
    public  void m2(){


        System.out.println("this is before hook m2 method");
    }

    @After
    public  void tearDown(){


        System.out.println("this is after hook tear down method");
    }
    @Given("User pass the web url on browser")
    public void user_pass_the_web_url_on_browser() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("this is background keyword");
    }

    @Given("User provide the input for age")
    public void user_provide_the_input_for_age() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("proving the age value");
        age = 20;


    }

    @When("User validating age greater eighteen")
    public void user_validating_age_greater() {
        // Write code here that turns the phrase above into concrete actions

        if(age>18){

            result = true;
        }
        else
        {
            result = false;
        }

    }

    @Then("it should be greater eighteen")
    public void it_should_be_greater() {
        // Write code here that turns the phrase above into concrete actions
       // Assert.assertTrue(result);



    }



    @Given("User provide the input for username as {string} and password as {string}")
    public void user_provide_the_input_for_username_as_and_password_as(String username, String password) {
        // Write code here that turns the phrase above into concrete actions

        System.out.println(username);
        System.out.println(password);
    }

    @When("User validating home page after successful login attempt as {int}")
    public void user_validating_home_page_after_successful_login_attempt_as(Integer attempt) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(attempt);
    }

    @Then("it should be logged in")
    public void it_should_be_logged_in() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("this is logged in method");

    }


    @Given("User provide the input for {string} and  {string}")
    public void user_provide_the_input_for_and(String string, String string2) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(string);
        System.out.println(string2);
    }




    @Given("User provide the input for age, dob , mobileno, and email")
    public void user_provide_the_input_for_age_dob_mobileno_and_email(io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        String mobileno = dataTable.row(0).get(1);
        System.out.println(mobileno);
        System.out.println(dataTable.row(0).get(1));
        System.out.println(dataTable.row(0).get(2));
        System.out.println(dataTable.row(0).get(3));





    }
}
